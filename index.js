// console.log("hello")

// Discussion #1

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

/*
//  Using "keyup" event: makikinig js na may ipinasok sa html na content
// DOM manipulation
txtFirstName.addEventListener('keyup', (event) => {
	//innerHTML kung saan nag lalagay ng content
	spanFullName.innerHTML = txtFirstName.value;

})

// para lumabas sa console yung mga iniinput na data sa input
txtFirstName.addEventListener('keyup', (event) => {
	//  The Document where our JS is connected
	console.log(event.target);
	//  The value that our "keyup" event listened
	console.log(event.target.value);
})
*/


 // Discussion #2
 const updateFullName = () =>{
 	let firstName = txtFirstName.value;
 	let lastName = txtLastName.value;

 	spanFullName.innerHTML = `${firstName} ${lastName}`;
 }

 txtFirstName.addEventListener('keyup', updateFullName);
 txtLastName.addEventListener('keyup', updateFullName);
